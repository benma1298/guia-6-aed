
Guia 6 Algoritmos y Estructura de Datos
Codigo Dijkstra

Autor: Benjamin Martin

Dada una matriz de valores de las distancias (que se leen desde la terminal)
se implementa el Algortimo de Dijkstra, el cual encuentra la distancia minima
entre un vertice origen y los otros vertices de la digrafica.

Al iniciar el programa se pide al usuario el ingreso por terminal de las distancias
entre los vertices solicitados.

A partir de estos datos el Programa, al finalizar, entrega el Grafo resultante y
el contenido del arreglo D[].

Para iniciar el Programa en cuestion usar el comando "make" desde la terminal
y luego de esto, escribir el nombre de la aplicacion "./dijkstra"

Paso 1: digitar el comando "make" en la terminal 
Paso 2: digitar "./dijkstra"
Paso 3: Ingresar la dimension de la matriz.
Paso 4: Ingresar los nombres de los nodos.
Paso 5: Determinar las distancias.
