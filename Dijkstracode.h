
#ifndef DIJKSTRACODE_H
#define DIJKSTRACODE_H
#include <iostream>

using namespace std;

class Dijkstracode{
    private:
    int **matriz;
    
    public:
    Dijkstracode();

    void creaMatriz(int cantidadNodos, string nodos[]);
    void nombrarNodos(int cantidadNodos, string nodos[]);
    void programa(int cantidadNodos);

    void inicializar_vector_D(int D[], int cantidadNodos);
    void inicializar_vector_caracter(char vector[], int cantidadNodos);
    void aplicar_dijkstra(char V[], char S[], char VS[], int D[], int cantidadNodos);
    void actualizar_pesos(int D[], char VS[], char V[], char v, int cantidadNodos);
    void agrega_vertice_a_S(char S[], char vertice, int cantidadNodos);
    void actualizar_VS(char V[], char S[], char VS[], int cantidadNodos);
    void leer_nodos(char vector[], int cantidadNodos);
    void imprimir_vector_caracter(char vector[], char *, int cantidadNodos);
    void imprimir_vector_entero(int vector[], int cantidadNodos);

    void imprimeMatriz(int cantidadNodos);
    void imprimeGrafo(int cantidadNodos, string nodos[]);

    int calcular_minimo(int dw, int dv, int mvw);
    int elegir_vertice(char VS[], int D[], char V[], int cantidadNodos);
    int buscar_indice_caracter(char V[], char caracter, int cantidadNodos);
    int busca_caracter(char c, char vector[], int cantidadNodos);
};
#endif