#include <iostream>
#include "Dijkstracode.h"

using namespace std;


int main(int argc, char **argv){

    int op = 0;
    int dimension = 0;

    Dijkstracode *dijkstracode = new Dijkstracode();

    cout << "Determine la dimension de la matriz: ";
    cin >> dimension;
    string nodos[dimension];
    
    dijkstracode -> nombrarNodos(dimension, nodos);
    dijkstracode -> creaMatriz(dimension, nodos);

    do{
        cout << "Opciones del Programa: " << endl;
        cout << "1.- Correr el Programa." << endl;
        cout << "2.- Imprimir Matriz." << endl;
        cout << "3.- Imprimir Grafo." << endl;
        cout << "0.- Finalizar Programa." << endl;

        cout << "Ingrese el numero de la Opcion que desea: ";
        cin >> op;

        switch (op)
        {
        case 1:
            dijkstracode -> programa(dimension);
            break;
        case 2:
            dijkstracode -> imprimeMatriz(dimension);
            break;
        case 3:
            dijkstracode -> imprimeGrafo(dimension, nodos);

        default:
            break;
        }
    }while(op !=0);

    return 0;
}