
#include <iostream>
#include <fstream>
#include "Dijkstracode.h"
#define TRUE 0
#define FALSE 1

using namespace std;

Dijkstracode::Dijkstracode(){

}

//En la siguiente funcion, el primer ciclo for se encarga de dar inicio
//a la matriz y el segundo ciclo for, de rellenar la matriz en cuestion.
void Dijkstracode::creaMatriz(int cantidadNodos, string nodos[]){
    this -> matriz = new int*[cantidadNodos];
    int dis;
    for(int i = 0; i < cantidadNodos; i++){
        this -> matriz[i] = new int[cantidadNodos];
    }
    for(int i = 0; i < cantidadNodos; i++){
        cout << endl;
        cout << "Nodo en uso: " << nodos[i];
        cout << endl;
        for(int j = 0; j < cantidadNodos; j++){
            if(i != j){
                cout << "Ingrese la distancia correspondiente al nodo " << nodos[j]<< ": ";
                cin >> dis;
                this -> matriz[i][j] = dis;
            }else{
                this -> matriz[i][j] = 0;
            }
        }
    }
}

// Esta funcion se encarga de que el usuario le otorgue nombres
// a la cantidad de nodos ingresados al programa.
void Dijkstracode::nombrarNodos(int cantidadNodos, string nodos[]){
    string nodo;
    int i;
    for(i = 0; i < cantidadNodos; i++){
        cout << endl;
        cout << "Nombre al nodo " << i+1 << ": ";
        cin >> nodo;
        nodos[i] = nodo;
    }
}

// Esta funcion imprime el contenido de una matriz bidimensional de enteros.
void Dijkstracode::imprimeMatriz(int cantidadNodos){
    int i, j;
    for(i = 0; i < cantidadNodos; i++){
        for(j = 0; j < cantidadNodos; j++){
            cout << this -> matriz[i][j] << " ";
            cout << "\t";
        }
        cout << endl;
    }
}

// Funcion que crea el grafo a partir del metodo imprimir_grafo.
void Dijkstracode::imprimeGrafo(int cantidadNodos, string nodos[]){
    int i, j;
    ofstream fp;
    fp.open("grafo.txt");
    fp << "digraph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=green];" << endl;

    for(i = 0; i < cantidadNodos; i++){
        for(j = 0; j < cantidadNodos; j++){
            // evalua la diagonal principal.
            if(i != j){
                if(matriz[i][j] > 0){
                    fp << "\n" << '"' << nodos[i] << '"' << "->" << '"' << nodos[j] << '"' << "[label=" << this -> matriz[i][j] << "];" << endl;
                }
            }
        }
    }
    fp << "}" << endl;
    fp.close();
    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}

void Dijkstracode::programa(int cantidadNodos){
    int D[cantidadNodos];
    char V[cantidadNodos];
    char S[cantidadNodos];
    char VS[cantidadNodos];
    
    inicializar_vector_caracter(V, cantidadNodos);
    inicializar_vector_caracter(S, cantidadNodos);
    inicializar_vector_caracter(VS, cantidadNodos);
    leer_nodos(V, cantidadNodos);
    aplicar_dijkstra(V, S, VS, D, cantidadNodos);
}

//Esta funcion copia el contenido inicial a D[] desde la matriz M[][].
void Dijkstracode::inicializar_vector_D(int D[], int cantidadNodos){
    int col;
    for(col = 0; col < cantidadNodos; col++){
        D[col] = this -> matriz[0][col];
    }
}

//Esta funcion inicializa con espacios el arreglo de caracteres.
void Dijkstracode::inicializar_vector_caracter(char vector[], int cantidadNodos){
    int col;
    for(col = 0; col < cantidadNodos; col++){
        vector[col] = ' ';
    }
}

//La siguiente funcion aplica el algoritmo.
void Dijkstracode::aplicar_dijkstra(char V[], char S[], char VS[], int D[], int cantidadNodos){
    int i;
    int v;
    // inicializar vector D[] segun datos de la matriz M[][]
    // estado inicial.
    inicializar_vector_D(D, cantidadNodos);

    //
    cout << "\n----------Estados Iniciales----------\n" << endl;
    imprimeMatriz(cantidadNodos);
    cout << endl;
    imprimir_vector_caracter(S, "S", cantidadNodos);
    imprimir_vector_caracter(VS, "VS", cantidadNodos);
    imprimir_vector_entero(D, cantidadNodos);
    cout << "-------------------------------------\n\n";

    // agrega primer vertice.
    cout << "> agrega el primer valor V[0] a S[] y actualiza VS[]\n\n";
    agrega_vertice_a_S(S, V[0], cantidadNodos);
    imprimir_vector_caracter(S, "S", cantidadNodos);
    //
    actualizar_VS(V, S, VS, cantidadNodos);
    imprimir_vector_caracter(VS, "VS", cantidadNodos);
    imprimir_vector_entero(D, cantidadNodos);
    //
    for(i = 1; i < cantidadNodos; i++){
        // se elige un vertice en v de VS[] tal que D[v] sea el minimo
        cout << "\n > elige vertice menor en VS[] segun valores en D[]\n";
        cout << "> lo agrega a S[] y actualiza VS[]\n";
        v = elegir_vertice(VS, D, V, cantidadNodos);
        //
        agrega_vertice_a_S(S, v, cantidadNodos);
        imprimir_vector_caracter(S, "S", cantidadNodos);
        //
        actualizar_VS(V, S, VS, cantidadNodos);
        imprimir_vector_caracter(VS, "VS", cantidadNodos);
        //
        actualizar_pesos(D, VS, V, v, cantidadNodos);
        imprimir_vector_entero(D, cantidadNodos);
    }
}

// Esta funcion actualiza los valores del vector D llamando a la
// funcion calcular_minimo con los indices v y w.
void Dijkstracode::actualizar_pesos(int D[], char VS[], char V[], char v, int cantidadNodos){
    int i = 0;
    int indice_w, indice_v;
    cout << "\n> actualiza pesos en D[]\n";

    indice_v = buscar_indice_caracter(V, v, cantidadNodos);
    while(VS[i] != ' '){
        if(VS[i] != v){
            indice_w = buscar_indice_caracter(V, VS[i], cantidadNodos);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], this -> matriz[indice_v][indice_w]);
        }
        i++;
    }
}

// La siguiente funcion calcula el valor minimo entre dw y la suma dv + mvw.
int Dijkstracode::calcular_minimo(int dw, int dv, int mvw){
    int min = 0;

    if(dw == -1){
        if(dv != -1 && mvw != -1)
            min = dv + mvw;
        else
            min = -1;
    } else{
        if(dv != -1 && mvw != -1){
            if(dw <= (dv + mvw))
                min = dw;
            else
                min = (dv + mvw);
        }
        else
            min = dw;
    }
    cout << "dw: " << dw << " dv: " << dv << " mvw: " << mvw << " min: " << min << endl;
    return min;
}

// Esta funcion agrega un vertice a S[]
void Dijkstracode::agrega_vertice_a_S(char S[], char vertice, int cantidadNodos){
    int i;

    // recorre buscando un espacio vacio.
    for(i = 0; i < cantidadNodos; i++){
        if(S[i] == ' '){
            S[i] = vertice;
            return;
        }
    }
}

// Funcion que elige vertice con menor peso en VS[]
// busca su peso en D[].
int Dijkstracode::elegir_vertice(char VS[], int D[], char V[], int cantidadNodos){
    int i = 0;
    int menor = 0;
    int peso;
    int vertice;

    while(VS[i] != ' '){
        peso = D[buscar_indice_caracter(V, VS[i], cantidadNodos)];
        //descarta valores infinitos (-1) y 0.
        if((peso != -1) && (peso != 0)){
            if(i == 0){
                menor = peso;
                vertice = VS[i];
            } else{
                if(peso < menor){
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }
        i++;
    }
    cout << "\nvertice: " << vertice << "\n\n";
    return vertice;
}

// Funcion que retorna el indice del caracter consultado.
int Dijkstracode::buscar_indice_caracter(char V[], char caracter, int cantidadNodos){
    int i;
    for(i = 0; i < cantidadNodos; i++){
        if(V[i] == caracter)
        return i;
    }
    return i;
}

// Esta funcion busca la aparicion de un caracter en un vector de caracteres.
int Dijkstracode::busca_caracter(char c, char vector[], int cantidadNodos){
    int j;
    for(j = 0; j < cantidadNodos; j++){
        if(c == vector[j]){
            return TRUE;
        }
    }
    return FALSE;
}

//Funcion que actualiza VS[] cada ves que agrega un elemento a S[].
void Dijkstracode::actualizar_VS(char V[], char S[], char VS[], int cantidadNodos){
    int j;
    int k = 0;

    inicializar_vector_caracter(VS, cantidadNodos);

    for(j = 0; j < cantidadNodos; j++){
        // por cada caracter de V[] evalua si esta en S[],
        // Sino esta, lo agrega a VS[].
        if(busca_caracter(V[j], S, cantidadNodos) != TRUE){
            VS[k] = V[j];
            k++;
        }
    }
}

// La siguiente funcion lee los datos de los nodos.
// inicializa utilizando codigo ASCII.
void Dijkstracode::leer_nodos(char vector[], int cantidadNodos){
    int i;
    int inicio = 97;

    for(i = 0; i < cantidadNodos; i++){
        vector[i] = inicio + i;
    }
}

// Funcion que imprime el contenido de un vector de caracteres.
void Dijkstracode::imprimir_vector_caracter(char vector[], char *nomVector, int cantidadNodos){
    int i;

    for(i = 0; i < cantidadNodos; i++){
        cout << nomVector << "[" << i << "]: " << vector[i] << endl;
    }
    cout << endl;
}

// Funcion que imprime el contenido del vector D
void Dijkstracode::imprimir_vector_entero(int vector[], int cantidadNodos){
    int i;
    
    for(i = 0; i < cantidadNodos; i++){
        cout << "D[" << i << "]: " << vector[i] << endl;
    }
    cout << endl;
}

